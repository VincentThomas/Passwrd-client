import { flags } from '@oclif/command';
import Command from '../base/whoami';
import { textSync } from 'figlet';

export default class Whoami extends Command {
	static description = 'This will show who is logged in';

	static examples = [
		`$ passwrd whoami

[Logged in user]
		`
	];

	static flags = {
		help: flags.help({ char: 'h' })
	};

	async catch(err: any) {
		return await super.catch(err);
	}

	async run() {
		if (this.get.jwt === null) {
			this.log('INFO', true)('You are currenty logged out\n');
			this.exit();
		}

		const { data: user } = await this.get.user();
		console.log(textSync(user.username));
	}
}
