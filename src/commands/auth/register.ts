import { flags } from '@oclif/command';
import Command from '../../base/auth';
import { gray, green, cyan } from 'kleur';
import * as qr from 'qrcode-terminal';

export default class AuthRegister extends Command {
	static description = 'Create an account with this command';

	static examples = [
		`$ passwrd auth:register

${cyan('?')} Type username ...
${cyan('?')} Type password ...

${green('SUCCESS')} Done!
	`
	];

	static flags = {
		help: flags.help({ char: 'h' }),
		username: flags.string({
			char: 'u',
			description: 'This is the username to log in with',
			required: false
		})
	};
	private flags = this.parse(AuthRegister).flags;

	doneHint() {
		this.sendHint('manage website credentials', 'cre', false);
	}

	async run() {
		const username: string = !this.flags.username
			? await this.get.username()
			: this.flags.username;

		const password = await this.get.password();
		await this.func
			.register(username, password)
			.catch(this.errors.userNotExists);
		const { data } = await this.func.login(username, password);
		const jwt = data.access_token;
		console.log();
		const { logIn }: any = await this.prompt({
			type: 'confirm',
			message: 'Autologin?',
			name: 'logIn'
		});
		if (logIn) {
			this.set.jwt = jwt;
		}
		const { twofa }: any = await this.prompt({
			type: 'confirm',
			message: '2-Factor Authentication?',
			name: 'twofa'
		});
		if (twofa) {
			const { data }: any = await this.func['2fa'].create();
			qr.generate(data.url, { small: true });
			this.log(
				'INFO',
				true
			)(
				`Scan this QR-code in google authenticator\n${gray(
					`* Can't add via qr-code? Type this code instead: ${data.token}`
				)}`
			);

			const { code }: any = await this.prompt({
				type: 'input',
				message: 'Type the confirmation code in Google Authenticator',
				name: 'code'
			}).catch(async (e: Error) => {
				await this.func['2fa'].remove();
			});
			const response = await this.func['2fa'].verify(code, {
				username,
				password
			});
			if (!response.data.verified) {
				await this.func['2fa'].remove();
				this.errors.wrong2faCode();
			} else {
				this.set.jwt = response.data.access_token;
			}
		}
		this.log('SUCCESS', true)(`User ${username} added!\n`);
		if (logIn) this.doneHint();
	}
}
