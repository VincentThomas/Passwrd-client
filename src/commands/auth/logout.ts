import { flags } from '@oclif/command';
import Command from '../../base/auth';
import { cyan } from 'kleur';

export default class AuthLogout extends Command {
	static description = 'This command logs you out';

	static examples = [
		`$ passwrd auth:logout

${cyan('INFO')} You are now logged out!
`
	];

	static flags = {
		help: flags.help({ char: 'h' })
	};

	async run() {
		this.jwt.clear();
		this.log('INFO', false)('You are now logged out!');
	}
}
