import { flags } from '@oclif/command';
import Command from '../../../base/auth';
import { green, cyan } from 'kleur';
export default class AuthLogin extends Command {
	static description =
		'Remove 2fa for your account! Beware, this will make your account much less secure!';

	static examples = [
		`$ passwrd auth:2fa:rem

${cyan('?')} Type username ...
${cyan('?')} Type password ...
${cyan('?')} Enter security code ...

${green('SUCCESS')} Done!
`
	];

	static flags = {
		help: flags.help({ char: 'h' })
	};

	async init() {
		const username = await this.get.username();

		const password = await this.get.password();
		const result = await this.func.login(username, password);
		if (result.data.code === 401) this.errors.userNotExists();

		if (result.data.access_token) {
			this.log('ERROR', true)("You don't have 2-factor Auth!");
			this.exit();
		} else {
			const code = await this.get.code();
			const result = await this.func['2fa'].verify(code, {
				username,
				password
			});
			if (result.data.verified) this.set.jwt = result.data.access_token;
			else this.errors.wrong2faCode();
		}
	}

	async run() {
		const res = await this.func['2fa'].remove();
		if (res.code === 201) this?.log('SUCCESS', true)('Done!');
	}
}
