import { flags } from '@oclif/command';
import Command from '../../../base/auth';
import { green, gray, cyan } from 'kleur';
import * as qr from 'qrcode-terminal';

export default class AuthLogin extends Command {
	static description = 'This will add 2 Factor Authentication to your account';

	static examples = [
		`$ passwrd auth:2fa:add

${cyan('?')} Type username ...
${cyan('?')} Type password ...

${green(`SUCCESS`)} Logged in!
`
	];

	static flags = {
		help: flags.help({ char: 'h' })
	};

	private username: string = '';
	private password: string = '';

	async init() {
		this.username = await this.get.username();
		this.password = await this.get.password();
		const result = await this.func.login(this.username, this.password);

		if (result.data.code === 401) this.errors.userNotExists();

		if (result.data.msg === '2fa_needed') {
			this.log('ERROR', true)('You already have 2-factor Auth!');
			this.exit();
		} else this.set.jwt = result.data.access_token;
	}

	async run() {
		process.stdout.write('\n\n');

		const { isReady }: any = await this.prompt({
			type: 'confirm',
			message: 'Are you sure you want to setup 2fa?',
			name: 'isReady'
		});

		if (!isReady) {
			this.log('INFO', true)('ok');
			this.exit();
		}

		const { data }: any = await this.func['2fa'].create();
		process.stdout.write('\n\n');
		qr.generate(data.url, { small: true });
		this.log(
			'INFO',
			true
		)(
			`Scan this QR-code in google authenticator\n${gray(
				`* Can't add via qr-code? Type this code instead: ${data.token}\n`
			)}`
		);
		const { code }: any = await this.prompt({
			type: 'input',
			message: 'Type the confirmation code in Google Authenticator',
			name: 'code'
		}).catch(async () => {
			await this.func['2fa'].remove();
		});

		const response = await this.func['2fa'].verify(code, {
			username: this.username,
			password: this.password
		});
		if (!response.data.verified)
			this.func['2fa'].remove().then(() => {
				this.errors.wrong2faCode();
			});
		else this.set.jwt = response.data.access_token;
		this.log('SUCCESS', true)(`Done!\n`);
	}
}
