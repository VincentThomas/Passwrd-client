import { flags } from '@oclif/command';
import Command from '../../base/auth';
import { green, cyan } from 'kleur';

export default class AuthLogin extends Command {
	static description =
		'Logs you in, you will automatically logged out after 15 mins';

	static examples = [
		`$ passwrd auth:login

${cyan('?')} Type username ...
${cyan('?')} Type password ...

${green(`SUCCESS`)} Logged in!
`
	];

	static flags = {
		help: flags.help({ char: 'h' }),
		username: flags.string({
			char: 'u',
			description: 'This is the username to log in with',
			required: false
		})
	};

	private username: string = '';
	private password: string = '';
	private result: any = {};

	async init() {
		if (this.get.jwt) {
			this.log('WARNING', true)('You are already logged in! (logout first)\n');
			const { shouldLogout }: any = await this.prompt([
				{
					// @ts-expect-error
					type: 'toggle',
					message: 'You are already logged in! Logout now?',
					enabled: 'Yes',
					name: 'shouldLogout',
					disabled: 'No'
				}
			]);
			if (shouldLogout) {
				this.jwt.clear();
				this.log('SUCCESS', true)('Done!\n');
			} else {
				this.log('INFO', true)('ok, bye!\n');
				this.exit();
			}
		}
		const { flags } = this.parse(AuthLogin);

		this.username = flags.username || (await this.get.username());
		this.password = await this.get.password();

		this.result = await this.func
			.login(this.username, this.password)
			.catch(e => {
				if (e.code === 401) {
					this.errors.userNotExists();
				}
			});
	}

	async run() {
		if (this.result.data.msg === '2fa_needed') {
			const code = await this.get.code();
			const response = await this.func['2fa'].verify(code, {
				username: this.username,
				password: this.password
			});
			if (response.data.verified) {
				this.set.jwt = response.data.access_token;
				this.log('SUCCESS', true)("You're in!");
				this.exit();
			} else this.errors.wrong2faCode();
		} else {
			this.set.jwt = this.result.data.access_token;
			this.log('SUCCESS', true)("You're in!");
		}
	}
}
