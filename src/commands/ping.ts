import { flags } from '@oclif/command';
import Command from '../base/ping';

export default class Ping extends Command {
	static description = 'This will check the status of the server.';

	async run() {
		await this.ping()
			.then(() => {
				this.log('INFO', true)('The server is running correctly!\n');
			})
			.catch(e => {
				this.log('ERROR', true)('The server is not currently running\n');
			});
	}
}
