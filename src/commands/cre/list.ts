import { flags } from '@oclif/command';
import Command from '../../base/cre';
import Table from 'cli-table3';

function randomItemFromArray(array: Array<any>): any {
	return array[Math.floor(Math.random() * array.length)];
}

export default class GetCre extends Command {
	static description = 'Gets you all the credentials!';

	static examples = [
		`$ passwrd cre:get

[Table full of data]
`
	];

	static flags = {
		help: flags.help({ char: 'h' }),
		all: flags.boolean({
			description: 'This shows the passwords',
			required: false
		})
	};

	private flags = this.parse(GetCre).flags;

	async init() {
		if (!this.get.jwt) {
			this.log('ERROR', true)('You need to be logged in to view accounts!');
			this.exit();
			// const username = await this.get.username();

			// const password = await this.get.password();

			// const result = await this.func.login(username, password);
			// if (result.code === 401) this.errors.userNotExists();
			// else if (result.data?.msg !== '2fa_needed')
			// 	this.get.jwt = result.data.access_token;
			// else {
			// 	const code = await this.get.code();
			// 	const res = await this.func['2fa'].verify(code, {
			// 		username,
			// 		password
			// 	});
			// 	if (!res.data.verified) this.errors.wrong2faCode();
			// 	else this.set.jwt = res.data.access_token;
			// }
		}
	}

	async run() {
		const res = await this.func.cre.all();
		if (res.data.length === 0) {
			this.log('WARNING', false)("You don't have any websites to display!");
			this.exit();
		}
		const headers = (
			res.data.length === 0 ? null : res.data.map(Object.keys)[0]
		).map((v: string) => v.replace(/^\w/, u => u.toUpperCase()));
		const value = res.data.map((v: any) => {
			let password = v.password;
			if (!this.flags.all) {
				password = '*'.repeat(password.length);
			}
			v.password = password;
			return Object.values(v);
		});
		const table = new Table({ head: headers });
		table.push(...value);
		process.stdout.write(table.toString() + '\n');
		let websitesArray: string[] = [];
		value.map((v: string) => {
			websitesArray.push(v[0]);
		});
		if (!this.flags.all) {
			this.log(
				'INFO',
				true
			)("To display passwords, include the '--all' (short '-a') flag\n");
		} else {
			this.log(
				'INFO',
				true
			)(
				`All credentials (except website names ex. ${randomItemFromArray(
					websitesArray
				)}) are encrypted\n`
			);
		}
	}
}
