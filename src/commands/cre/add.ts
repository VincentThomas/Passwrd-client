import { flags } from '@oclif/command';
import Command from '../../base/cre';
import { green, cyan } from 'kleur';

export default class AddCre extends Command {
	static description = "Add credentials so you don't have to remember them!";

	static examples = [
		`$ passwrd cre:add

${cyan('?')} What website  to store ...
${cyan('?')} What username to store ...
${cyan('?')} What password to store ...

${green(`SUCCESS`)} Website has been created!
`
	];

	static flags = {
		help: flags.help({ char: 'h' }),
		website: flags.string({
			char: 'w',
			description: 'What website you want to add',
			required: false,
			dependsOn: ['username']
		}),
		username: flags.string({
			char: 'u',
			description: 'what username that should be added',
			required: false,
			dependsOn: ['website']
		})
	};

	async init() {
		if (!this.get.jwt) {
			{
				this.log('ERROR', true)('You need to be logged in to view accounts!');
				this.exit();
				// const username = await this.get.username();
				// const password = await this.get.password();

				// const data = await this.func.login(username, password);

				// if (data?.access_token) {
				// 	this.set.jwt = data.access_token;
				// 	this.log('SUCCESS', true)("You're in!");
				// 	return;
				// } else if (data.msg === '2fa_needed') {
				// 	const code = await this.get.code();
				// 	const { data } = await this.func['2fa'].verify(code, {
				// 		username,
				// 		password
				// 	});
				// 	if (!data.verified) this.errors.wrong2faCode();
				// 	else if (typeof data.access_token === 'string')
				// 		this.set.jwt = data.acecss_token;
			}
			// await super.init().catch(() => {});
		}
	}

	async run() {
		const { flags } = this.parse(AddCre);

		let {
			username: fusername,
			password: fpassword,
			website: fwebsite
		}: any = flags;
		const website = fwebsite || (await this.get.website());
		const username = fusername || (await this.get.username());
		const password = fpassword || (await this.get.password());
		const result = await this.func.cre
			.create({
				website,
				username,
				password
			})
			.catch((res: { code: number }) => {
				if (res.code === 409) {
					this.log('ERROR', true)('Website already exists');
					this.exit(0);
				}
			});
		if (result.code === 201) this.log('SUCCESS', true)('Done!');
		else this.log('ERROR', true)('Unkown Error...\n');
	}
}
