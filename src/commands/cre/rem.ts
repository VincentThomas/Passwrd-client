import { flags } from '@oclif/command';
import Command from '../../base/cre';
import { from } from 'rxjs';
import { map } from 'rxjs/operators';
import { green, cyan } from 'kleur';
export default class DelCre extends Command {
	static description = 'Delete one/many saved credentials';

	static examples = [
		`$ passwrd cre:del

${cyan('?')} Which websites do you want to delete ...

${green('SUCCESS')} Deleted websites
`
	];

	static flags = { help: flags.help({ char: 'h' }) };

	async init(): Promise<void | never> {
		if (!this.get.jwt) {
			{
				this.log('ERROR', true)('You need to be logged in to view accounts!');
				this.exit();
				// const username = await this.get.username();
				// const password = await this.get.password();
				// const res = await this.func
				// 	.login(username, password)
				// 	.catch(this.errors.userNotExists);
				// const [msg, error] = this.func.handleLogin(res);
				// if (error === '2fa_needed') {
				// 	const code = await this.get.code();
				// 	const { data } = await this.func['2fa'].verify(code, {
				// 		username,
				// 		password
				// 	});
				// 	data.verified
				// 		? (this.get.jwt = data.access_token)
				// 		: this.errors.wrong2faCode();
				// } else {
				// 	this.get.jwt = msg as string;
			}
		}
	}

	async run(): Promise<void | never> {
		const res = await this.func.cre.all().catch(e => {
			console.log(e);
		});
		if (res.length === 0) {
			this.log('WARNING', false)("You don't have any websites do delete!");

			this.exit();
		}

		let arr: any[] = [];
		from(res.data)
			.pipe(map((v: any) => v.website))
			.subscribe((v: string) => arr.push(v));

		res.data.map((v: { username?: string; password?: string }) => {
			delete v.username;
			delete v.password;
		});
		const { websites: selectedWebsites }: any = await this.prompt([
			{
				type: 'multiselect',
				message: 'Which websites do you want to delete?',
				name: 'websites',
				choices: arr
			}
		]);

		if (selectedWebsites.length === 0) {
			this.log(
				'WARNING',
				true
			)('You filled out 0 items, not doing anything...');
			this.exit();
		}

		const allWebsites: string[] = [];
		res.data.map((v: { website: string }) => {
			allWebsites.push(...Object.values(v));
		});

		function findDuplicates(arr: string[]): string[] {
			const filtered = arr.filter((item, index) => arr.indexOf(item) !== index);
			return [...new Set(filtered)];
		}

		const duplicates = findDuplicates([...allWebsites, ...selectedWebsites]);

		await this.func.cre
			.remove(duplicates)
			.then(() => {
				this.log('SUCCESS', true)('Deleted websites');
			})
			.catch(this.errors.unkownError);
	}
}
