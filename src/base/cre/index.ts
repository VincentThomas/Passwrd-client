import Command from '..';
import { all, remove, create } from '../shared/cre';

import { get, set } from '../shared/jwt';
import { username, password, code, website } from '../shared/templates';

export default abstract class Cre extends Command {
	public get = {
		get jwt(): any {
			return get();
		},
		set jwt(what: string) {
			set(what);
		},
		website,
		username,
		password,
		code
	};
	public set = {
		set jwt(what: string) {
			set(what);
		}
	};

	public func = {
		cre: { all, remove, create }
	};
}
