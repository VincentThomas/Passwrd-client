import axios from 'axios';
import { url } from '../getAPIurl';
import Command from '..';

import { get, set } from '../shared/jwt';

export default abstract class WhoAmIBase extends Command {
	public get = {
		user,
		get jwt(): string | null {
			return get();
		}
	};
	public set = {
		set jwt(what: string) {
			set(what as string);
		}
	};
	async catch(err: any) {
		if (err) await super.catch(err);
	}
}

async function user() {
	const { data: response } = await axios({
		method: 'GET',
		url: `${url}/u`,
		headers: { Authorization: `Bearer ${get()}` }
	});
	return response;
}
