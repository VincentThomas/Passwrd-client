import axios from 'axios';
import Command, { url } from '../';

export default abstract class Base extends Command {
	async ping() {
		const { data: response } = await axios.get(url).catch(e => {
			throw new Error('Server is not running...');
		});
		if (!(response.data.status === 'ok')) {
			throw new Error('Server is not running...');
		}
		return true;
	}
}
