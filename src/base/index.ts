import Command from '@oclif/command';
import { prompt } from 'enquirer';
import { cyan } from 'kleur';
import { wrong2faCode, userNotExists, unkownError } from './shared/errors';
import { log } from './shared/common';

export { url } from './getAPIurl';

export default abstract class Base extends Command {
	protected errors = {
		wrong2faCode,
		userNotExists,
		unkownError
	};

	protected prompt = prompt;

	public log = log;
}
