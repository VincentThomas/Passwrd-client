import { prompt } from 'enquirer';
import { prompt as Eprompt } from 'inquirer';

export async function website(): Promise<string> {
	const { website }: { website: string } = await prompt([
		{
			type: 'input',
			message: 'Type website',
			name: 'website'
		}
	]);
	return website;
}

export async function username(): Promise<string> {
	const { username }: { username: string } = await prompt([
		{
			type: 'input',
			message: 'Type username',
			name: 'username'
		}
	]);
	return username;
}

export async function password(): Promise<string> {
	const { password }: any = await Eprompt({
		type: 'password',
		message: 'Type password',
		name: 'password'
	});
	return password;
}

export async function code() {
	const { code }: any = await prompt([
		{
			type: 'input',
			name: 'code',
			message: 'Enter security code'
		}
	]);
	return code;
}
