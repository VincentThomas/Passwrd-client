import axios from 'axios';
import { get } from './jwt';
import { url } from '../getAPIurl';

export async function all() {
	const { data: response } = await axios({
		method: 'GET',
		url: `${url}/u/s`,
		headers: { Authorization: `Bearer ${get()}` }
	}).catch(e => {
		throw e.response;
	});
	return response;
}

export async function create(options: {
	website: string;
	username: string;
	password: string;
}) {
	const { data: response } = await axios({
		method: 'POST',
		url: `${url}/u/s`,
		data: options,
		headers: { Authorization: `Bearer ${get()}` }
	}).catch(e => {
		throw e.response.data;
	});
	return response;
}

export async function remove(websites: any) {
	const { data: response } = await axios({
		method: 'DELETE',
		url: `${url}/u/s`,
		data: websites,
		headers: {
			Authorization: `Bearer ${get()}`
		}
	}).catch(e => {
		throw e.response.data;
	});
	return response;
}
