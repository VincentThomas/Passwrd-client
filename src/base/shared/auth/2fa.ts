import axios from 'axios';
import { url } from '../..';
import { get } from '../jwt';

export async function verify(
	code: string,
	cre: { username: string; password: string }
) {
	try {
		const { data: response } = await axios({
			method: 'POST',
			url: `${url}/a/2fa/verify/${code}`,
			data: {
				username: cre.username,
				password: cre.password
			}
		});
		return response;
	} catch (e: any) {
		return e.response.data;
	}
}

export async function create() {
	const { data: response } = await axios({
		method: 'POST',
		url: `${url}/a/2fa/create`,
		headers: {
			Authorization: `Bearer ${get()}`
		}
	}).catch(e => e.response);
	return response;
}

export async function remove() {
	const res = await axios({
		method: 'DELETE',
		url: `${url}/a/2fa/remove`,
		headers: {
			Authorization: `Bearer ${get()}`
		}
	}).catch((e: any) => e.response.data);
	return res;
}
