import axios from 'axios';

import { url } from '../../getAPIurl';

export async function login(username: string, password: string) {
	const { data: res }: any = await axios({
		method: 'POST',
		url: `${url}/a/login`,
		data: {
			username,
			password
		}
	}).catch(e => {
		throw e.response.data;
	});
	return res;
}

export async function register(username: string, password: string) {
	const { data: res } = await axios({
		method: 'POST',
		url: `${url}/a/register`,
		data: {
			username,
			password
		}
	}).catch(e => {
		throw e.response.data;
	});
	return res.data;
}

export function handleLogin(
	response: any
): [string | undefined, undefined | string] {
	const { data } = response;
	if (!response.ok) return [undefined, response.data.msg];
	return data.msg === '2fa_needed'
		? [undefined, data.msg]
		: [data.access_token as string, undefined];
}
