import type { titleType } from '../../types';
import { bgCyan, bgGreen, bgRed, bgYellow, black } from 'kleur';

export function log(type: titleType, spaceBefore: boolean): Function {
	const tag = {
		INFO: bgCyan,

		SUCCESS: bgGreen,

		ERROR: bgRed,

		WARNING: bgYellow
	}[type](` ${black(type)} `);

	process.stdout.write(`${spaceBefore === true ? '\n' : ''}${tag}`);
	return (msg: string = ''): void | never => {
		process.stdout.write(` ${msg}${spaceBefore === false ? '\n' : ''}\n`);
	};
}
