import store from './store';
import { StoreModel } from 'src/types';

export function get(): string | null {
	const data = store.get('jwt') as StoreModel;
	const time = new Date().getTime();
	if (!data) return null;
	const { jwt: value } = data;
	if (!(data.storedAt + data.expiringIn > time)) {
		store.clear();
		return null;
	}
	return value;
}

export function set(jwt: string): void {
	store.set('jwt', {
		jwt,
		storedAt: new Date().getTime(),
		expiringIn: 900000
	});
}

export const clear = store.clear;
