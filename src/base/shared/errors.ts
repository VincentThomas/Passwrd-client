import type { titleType } from '../../types';
import { bgCyan, bgGreen, bgRed, bgYellow, black } from 'kleur';
import { log } from './common';

export function wrong2faCode(): never {
	log('ERROR', true)('Wrong 2FA Code!\n');
	process.exit(0);
}

export function userNotExists(): never {
	log('ERROR', true)('User does not exist\n');
	process.exit();
}

export function unkownError(): never {
	log('ERROR', true)('User does not exist\n');
	process.exit();
}
