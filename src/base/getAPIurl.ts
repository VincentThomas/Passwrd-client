import { readdirSync } from 'fs';
import { resolve } from 'path';

const rootPath = resolve(...[__dirname, '..', '..']);

export const url = readdirSync(rootPath).some((v: string) => v === 'src')
	? 'http://localhost:5000/passwrd'
	: 'https://api.v-thomas.xyz/passwrd';
