import Command from '../';
import { username, password, code } from '../shared/templates';
import { get, set } from '../shared/jwt';
import { cyan } from 'kleur';

import { login, register } from '../shared/auth';
import { create, remove, verify } from '../shared/auth/2fa';

export default abstract class Auth extends Command {
	// ? Accessors {{
	public get = {
		username,
		password,
		code,
		get jwt() {
			return get();
		}
	};
	public set = {
		set jwt(what: string) {
			set(what);
		}
	};
	public jwt = {
		clear() {}
	};
	// }}
	// ? Functions {{

	private TwofaFuncs = {
		verify,
		create,
		remove
	};

	sendHint(where: string, command: string, spaceBefore: boolean = true) {
		this.log(
			'INFO',
			spaceBefore
		)(`Psst! You can now ${where} using ${cyan(`passwrd ${command}`)}!`);
	}

	public func = {
		login,
		register,
		'2fa': this.TwofaFuncs
	};
	// ? }}
}
