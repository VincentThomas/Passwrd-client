import { Hook } from '@oclif/config';
import axios from 'axios';
import { url } from '../../base';

const hook: Hook<'init'> = async function ({ id }) {
	const serverNotRunning = () => {
		this.error(
			'Server is not running...\n' +
				'Please contact me at vincent.n.thomas@pm.me and say whats wrong!'
		);
	};
	if (id !== 'ping') await axios.get(url).catch(serverNotRunning);
};

export default hook;
