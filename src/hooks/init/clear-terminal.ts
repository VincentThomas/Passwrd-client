import { Hook } from '@oclif/config';

export default console.clear as Hook<'init'>;
