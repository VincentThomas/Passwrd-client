import { Hook } from '@oclif/config';
import updateNotifier from 'update-notifier';

const hook: Hook<'prerun'> = function ({ config }) {
	const { pjson } = config;

	const pkgJson = {
		name: pjson.name,
		version: pjson.version
	};

	const updater = updateNotifier({
		pkg: pkgJson,
		updateCheckInterval: 1
	});
	updater.config.set('path', config.configDir);

	updater.notify({ defer: false, isGlobal: true });
};

export default hook;
