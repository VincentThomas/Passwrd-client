import { Hook } from '@oclif/config';
import { Fonts, textSync } from 'figlet';
import { dim } from 'kleur';

function getFont(width: number): Fonts {
	if (width >= 91) {
		return 'Star Wars';
	} else if (width >= 56) {
		return 'Speed';
	} else if (width >= 47) {
		return 'Slant';
	} else if (width >= 40) {
		return 'Big';
	} else if (width >= 30) {
		return 'Rectangles';
	}
	return 'Slant';
}

const hook: Hook<'prerun'> = async props => {
	if (props.Command.id !== 'whoami')
		process.stdout.write(
			dim(textSync('Passwrd', getFont(process.stdout.columns || 100))) + '\n'
		);
};

export default hook;
