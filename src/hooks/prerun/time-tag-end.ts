import { Hook } from '@oclif/config';
import { cyan } from 'kleur';

const hook: Hook<'prerun'> = async function (opts) {
	console.time(cyan('Finished in'));

	process.on('exit', () => {
		console.timeEnd(cyan('Finished in'));
	});
};

export default hook;
