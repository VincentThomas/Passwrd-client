
export type titleType = 'INFO' | 'SUCCESS' | 'ERROR' | 'WARNING';

export interface StoreModel {
	jwt: string;
	storedAt: number;
	expiringIn: number;
}