# passwrd

![npm](https://img.shields.io/npm/v/passwrd?label=passwrd&style=for-the-badge)
![Downloads](https://img.shields.io/npm/dw/passwrd?style=for-the-badge)
[![License](https://img.shields.io/npm/l/passwrd?style=for-the-badge)](https://gitlab.com/VincentThomas/Passwrd-client/-/blob/main/package.json)

<!-- toc -->
* [passwrd](#passwrd)
* [Usage](#usage)
* [Commands](#commands)
<!-- tocstop -->

# Usage

<!-- usage -->
```sh-session
$ npm install -g passwrd
$ passwrd COMMAND
running command...
$ passwrd (-v|--version|version)
passwrd/0.1.6 win32-x64 node-v16.13.0
$ passwrd --help [COMMAND]
USAGE
  $ passwrd COMMAND
...
```
<!-- usagestop -->

# Commands

<!-- commands -->
* [`passwrd auth:2fa:add`](#passwrd-auth2faadd)
* [`passwrd auth:2fa:rem`](#passwrd-auth2farem)
* [`passwrd auth:login`](#passwrd-authlogin)
* [`passwrd auth:logout`](#passwrd-authlogout)
* [`passwrd auth:register`](#passwrd-authregister)
* [`passwrd autocomplete [SHELL]`](#passwrd-autocomplete-shell)
* [`passwrd commands`](#passwrd-commands)
* [`passwrd cre:add`](#passwrd-creadd)
* [`passwrd cre:list`](#passwrd-crelist)
* [`passwrd cre:rem`](#passwrd-crerem)
* [`passwrd help [COMMAND]`](#passwrd-help-command)
* [`passwrd ping`](#passwrd-ping)
* [`passwrd whoami`](#passwrd-whoami)

## `passwrd auth:2fa:add`

This will add 2 Factor Authentication to your account

```
USAGE
  $ passwrd auth:2fa:add

OPTIONS
  -h, --help  show CLI help

EXAMPLE
  $ passwrd auth:2fa:add

  ? Type username ...
  ? Type password ...

  SUCCESS Logged in!
```

_See code: [src/commands/auth/2fa/add.ts](https://gitlab.com/VincentThomas/Passwrd-client/blob/v0.1.6/src/commands/auth/2fa/add.ts)_

## `passwrd auth:2fa:rem`

Remove 2fa for your account! Beware, this will make your account much less secure!

```
USAGE
  $ passwrd auth:2fa:rem

OPTIONS
  -h, --help  show CLI help

EXAMPLE
  $ passwrd auth:2fa:rem

  ? Type username ...
  ? Type password ...
  ? Enter security code ...

  SUCCESS Done!
```

_See code: [src/commands/auth/2fa/rem.ts](https://gitlab.com/VincentThomas/Passwrd-client/blob/v0.1.6/src/commands/auth/2fa/rem.ts)_

## `passwrd auth:login`

Logs you in, you will automatically logged out after 15 mins

```
USAGE
  $ passwrd auth:login

OPTIONS
  -h, --help               show CLI help
  -u, --username=username  This is the username to log in with

EXAMPLE
  $ passwrd auth:login

  ? Type username ...
  ? Type password ...

  SUCCESS Logged in!
```

_See code: [src/commands/auth/login.ts](https://gitlab.com/VincentThomas/Passwrd-client/blob/v0.1.6/src/commands/auth/login.ts)_

## `passwrd auth:logout`

This command logs you out

```
USAGE
  $ passwrd auth:logout

OPTIONS
  -h, --help  show CLI help

EXAMPLE
  $ passwrd auth:logout

  INFO You are now logged out!
```

_See code: [src/commands/auth/logout.ts](https://gitlab.com/VincentThomas/Passwrd-client/blob/v0.1.6/src/commands/auth/logout.ts)_

## `passwrd auth:register`

Create an account with this command

```
USAGE
  $ passwrd auth:register

OPTIONS
  -h, --help               show CLI help
  -u, --username=username  This is the username to log in with

EXAMPLE
  $ passwrd auth:register

  ? Type username ...
  ? Type password ...

  SUCCESS Done!
```

_See code: [src/commands/auth/register.ts](https://gitlab.com/VincentThomas/Passwrd-client/blob/v0.1.6/src/commands/auth/register.ts)_

## `passwrd autocomplete [SHELL]`

display autocomplete installation instructions

```
USAGE
  $ passwrd autocomplete [SHELL]

ARGUMENTS
  SHELL  shell type

OPTIONS
  -r, --refresh-cache  Refresh cache (ignores displaying instructions)

EXAMPLES
  $ passwrd autocomplete
  $ passwrd autocomplete bash
  $ passwrd autocomplete zsh
  $ passwrd autocomplete --refresh-cache
```

_See code: [@oclif/plugin-autocomplete](https://github.com/oclif/plugin-autocomplete/blob/v0.3.0/src/commands/autocomplete/index.ts)_

## `passwrd commands`

list all the commands

```
USAGE
  $ passwrd commands

OPTIONS
  -h, --help              show CLI help
  -j, --json              display unfiltered api data in json format
  -x, --extended          show extra columns
  --columns=columns       only show provided columns (comma-separated)
  --csv                   output is csv format [alias: --output=csv]
  --filter=filter         filter property by partial string matching, ex: name=foo
  --hidden                show hidden commands
  --no-header             hide table header from output
  --no-truncate           do not truncate output to fit screen
  --output=csv|json|yaml  output in a more machine friendly format
  --sort=sort             property to sort by (prepend '-' for descending)
```

_See code: [@oclif/plugin-commands](https://github.com/oclif/plugin-commands/blob/v1.3.0/src/commands/commands.ts)_

## `passwrd cre:add`

Add credentials so you don't have to remember them!

```
USAGE
  $ passwrd cre:add

OPTIONS
  -h, --help               show CLI help
  -u, --username=username  what username that should be added
  -w, --website=website    What website you want to add

EXAMPLE
  $ passwrd cre:add

  ? What website  to store ...
  ? What username to store ...
  ? What password to store ...

  SUCCESS Website has been created!
```

_See code: [src/commands/cre/add.ts](https://gitlab.com/VincentThomas/Passwrd-client/blob/v0.1.6/src/commands/cre/add.ts)_

## `passwrd cre:list`

Gets you all the credentials!

```
USAGE
  $ passwrd cre:list

OPTIONS
  -h, --help  show CLI help
  --all       This shows the passwords

EXAMPLE
  $ passwrd cre:get

  [Table full of data]
```

_See code: [src/commands/cre/list.ts](https://gitlab.com/VincentThomas/Passwrd-client/blob/v0.1.6/src/commands/cre/list.ts)_

## `passwrd cre:rem`

Delete one/many saved credentials

```
USAGE
  $ passwrd cre:rem

OPTIONS
  -h, --help  show CLI help

EXAMPLE
  $ passwrd cre:del

  ? Which websites do you want to delete ...

  SUCCESS Deleted websites
```

_See code: [src/commands/cre/rem.ts](https://gitlab.com/VincentThomas/Passwrd-client/blob/v0.1.6/src/commands/cre/rem.ts)_

## `passwrd help [COMMAND]`

display help for passwrd

```
USAGE
  $ passwrd help [COMMAND]

ARGUMENTS
  COMMAND  command to show help for

OPTIONS
  --all  see all commands in CLI
```

_See code: [@oclif/plugin-help](https://github.com/oclif/plugin-help/blob/v3.2.10/src/commands/help.ts)_

## `passwrd ping`

This will check the status of the server.

```
USAGE
  $ passwrd ping
```

_See code: [src/commands/ping.ts](https://gitlab.com/VincentThomas/Passwrd-client/blob/v0.1.6/src/commands/ping.ts)_

## `passwrd whoami`

This will show who is logged in

```
USAGE
  $ passwrd whoami

OPTIONS
  -h, --help  show CLI help

EXAMPLE
  $ passwrd whoami

  [Logged in user]
```

_See code: [src/commands/whoami.ts](https://gitlab.com/VincentThomas/Passwrd-client/blob/v0.1.6/src/commands/whoami.ts)_
<!-- commandsstop -->
